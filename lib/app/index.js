var derby = require('derby')
  , app = derby.createApp(module)

app.get('/', function(page, model, params, next) {
  model.subscribe("state", function(err, result) {
    page.render()
  })
})

app.ready(function(model) {
})
