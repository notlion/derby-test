var http = require('http')
  , path = require('path')
  , express = require('express')
  , gzippo = require('gzippo')
  , derby = require('derby')
  , app = require('../app')
  , serverError = require('./serverError')

var expressApp = express()
  , server = module.exports = http.createServer(expressApp)

derby.use(derby.logPlugin)
var store = derby.createStore({ listen: server })

store.afterDb("set", "state.*", function(txn, doc, prevDoc, next) {
  var fixed = false // Set true to fix
  if(fixed) next()
  if(txn[1].charAt(0) != "#") { // Don't set on local changes.
    store.get("state.a", function(err, a) {
      store.get("state.b", function(err, b) {
        store.set("state.result", +a + +b)
      })
    })
  }
  if(!fixed) next()
})

var ONE_YEAR = 1000 * 60 * 60 * 24 * 365
  , root = path.dirname(path.dirname(__dirname))
  , publicPath = path.join(root, 'public')

expressApp
  .use(express.favicon())
  .use(gzippo.staticGzip(publicPath, { maxAge: ONE_YEAR }))
  .use(express.compress())
  .use(store.modelMiddleware())
  .use(app.router())
  .use(expressApp.router)
  .use(serverError(root))

expressApp.all('*', function(req) {
  throw '404: ' + req.url
})
